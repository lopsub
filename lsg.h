/*
 * Copyright (C) 2016 Andre Noll <maan@tuebingen.mpg.de>
 *
 * Licensed under the GPL v3, see https://www.gnu.org/licenses/gpl-3.0.html
 */

struct lsg_name {
	char *orig;
	char *sanitized;
	char *capitalized;
};

struct lsg_option {
	struct lsg_name name;
	char short_opt;
	char *summary;
	char *arg_info;
	char *arg_type;
	char *typestr;
	bool multiple, required, ignored;
	char *default_val;
	char *help;
	int num_values;
	char **values;
	char **value_literals;
	char **value_ids;
};

struct lsg_command {
	struct lsg_name name;
	char *purpose;
	char *description;
	char *closing;
	char *non_opts_name;
	char *synopsis;
	char *aux_info;
	struct lsg_option *options;
	int num_options;
};

struct lsg_section {
	struct lsg_name name;
	char *text;
};

struct lsg_suite {
	struct lsg_name name;
	char *caption; /* additional section heading (.SH), default: empty */
	char *title; /* defaults to suite name */
	char *mansect; /* default: 1 */
	char *date; /* default: current date */
	char *version_string; /* default: empty */
	char *manual_title; /* default: User commands */
	char *aux_info_prefix;
	char *aux_info_default;
	char *introduction, *conclusion;
	/* supercommand is #0, subcommands start at index 1 */
	struct lsg_command *commands;
	int num_subcommands;
	struct lsg_section *sections;
	int num_sections;
};

extern struct lsg_suite suite;

#ifndef STAGE1
#define OPT_RESULT(_name, _lpr) ((_lpr)->opt_result + LSG_LOPSUBGEN_LOPSUBGEN_OPT_ ## _name)
#define OPT_GIVEN(_name, _lpr) (OPT_RESULT(_name, (_lpr))->given)
#define OPT_STRING_VAL(_name, _lpr) (OPT_RESULT(_name, (_lpr))->value[0].string_val)
#endif /* STAGE1 */
