changequote(`«', `»')dnl
define(«IFNBLANK», «ifelse(translit(««$1»», «
	 »), «», «», «$2»)»)dnl
define(«DECL_ARGLIST», «ifelse(«$#», «2», «$1»,
	«$1, DECL_ARGLIST(shift(shift($@)))»)»)dnl
define(«LINK_COUNT», 0)
define(«STRIP_LEFT», «patsubst(«$1», «^\s+», «»)»)dnl
define(«STRIP_RIGHT», «patsubst(«$1», «\s+$», «»)»)dnl
define(«STRIP», «STRIP_LEFT(«STRIP_RIGHT(«$1»)»)»)dnl
ifelse(OUTPUT_MODE, «C», «dnl --------- C output
define(«COMPOUND_MEMBERS», «ifelse(«$#», «1», «»,
	«	$1; /* STRIP_RIGHT(«$2») */
COMPOUND_MEMBERS(shift(shift($@)))»)»)dnl
define(«DECL_ARG_TEXT», «ifelse(«$#», «1», «», «IFNBLANK(«$2», «
		$1: $2«»DECL_ARG_TEXT(shift(shift($@)))»)»)»)dnl
define(«DECLARE_FUNCTION», «/* $2 */
$6 $1(DECL_ARGLIST($4));
/*dnl
$3«»dnl
DECL_ARG_TEXT($4)dnl
IFNBLANK(«$5», «$5»)dnl
IFNBLANK(«$7», «
		Return ($6): STRIP_RIGHT(«$7»)»
)dnl
IFNBLANK(«$8», «$8»)dnl
*/»)dnl
define(«STATEMENT», «/* $2 */
$1;
IFNBLANK(«$3», «/*STRIP_RIGHT(«$3»)
*/dnl
»)dnl
»)dnl
define(«DECLARE_COMPOUND», «/* $2 */
$1 {
IFNBLANK(«$3», «/*STRIP_RIGHT(«$3»)
*/
»)dnl
COMPOUND_MEMBERS($4)dnl
};
IFNBLANK(«$5», «/*STRIP_RIGHT(«$5»)
*/
»)dnl
»)dnl
define(«VERBATIM_C», «$1»)dnl
», OUTPUT_MODE, «HTML», «dnl --------- HTML output
	define(«FIXUP_LT», «patsubst(«$1», «<», «&lt;»)»)
	define(«FIXUP_GT», «patsubst(«$1», «>», «&gt;»)»)
	define(«FIXUP_AMPERSAND», «patsubst(«$1», «&», «&amp;»)»)
	define(«HANDLE_EMPTY_LINES», «patsubst(«$1», «^\s*$», «</p><p>»)»)
	define(«FIXUP_HTML», «<p> FIXUP_AMPERSAND(«FIXUP_LT(«FIXUP_GT(
		«HANDLE_EMPTY_LINES(«$1»)»)»)») </p>»)
	define(«FORMAT_LIST_HTML», «ifelse(«$#», «1», «», «
		IFNBLANK(«$2», «<li> <tt>$1</tt>: $2 </li>
		FORMAT_LIST_HTML(shift(shift($@)))»)»
	)»)
	define(«ANCHOR», «
		<a name = "link_«»LINK_COUNT" </a>
	»)
	define(«HREF», «
		define(«LINK_COUNT», incr(LINK_COUNT))
		<a href = "«#»link_«»LINK_COUNT"> $1 </a>
	»)
	define(«DECLARE_FUNCTION», «
		divert
		<tr> <td> <tt> HREF(«$1») </tt> </td> <td> $2 </td> </tr>
		divert(«1»)
		ANCHOR
		<h1> <tt> $6 $1(DECL_ARGLIST($4)) </tt> </h1>
		<strong> FIXUP_HTML(«$2») </strong>
		FIXUP_HTML(«$3»)
		<ul> FORMAT_LIST_HTML($4) </ul>
		FIXUP_HTML(«$5»)
		IFNBLANK(«$7», «Return (<tt>$6</tt>):
			<ul><li> FIXUP_HTML(«$7») </lu></ul>»)
		FIXUP_HTML(«$8»)
		<hr>
	»)
	define(«STATEMENT», «
		divert
		<tr> <td> <tt> HREF(«$1») </tt> </td> <td> $2 </td>
		divert(«1»)
		ANCHOR
		<h1> <tt> $1 </tt> </h1>
		<strong> FIXUP_HTML(«$2») </strong>
		<p> FIXUP_HTML(«$3») </p>
		<hr>
	»)
	define(«DECLARE_COMPOUND», «
		divert
		<tr> <td> <tt> HREF(«$1») </tt> </td> <td> $2 </td>
		divert(«1»)
		ANCHOR
		<h1> <tt> $1 </tt> </h1>
		<strong> FIXUP_HTML(«$2») </strong>
		FIXUP_HTML(«$3»)
		Members:
		<ul> FORMAT_LIST_HTML($4) </ul>
		FIXUP_HTML(«$5»)
		<hr>
	»)
	define(«VERBATIM_C»)
	divert(«1») </table> <hr> divert
	<center> <h1> API Reference </h1> </center>
	<table>
», OUTPUT_MODE, «ROFF»,«dnl ---------- ROFF output
.TH
.SH SYNOPSIS
define(«VERBATIM_C»)
define(«FORMAT_LIST_ROFF», «ifelse(«$#», «1», «», «
IFNBLANK(«$2», «
.B "STRIP(«$1»)"
\- STRIP(«$2»)
.br dnl
FORMAT_LIST_ROFF(shift(shift($@)))»dnl
»))»)
define(«STATEMENT», «dnl
divert
.B "$1"
\- «$2»
.br dnl
divert(«1»)
.SS "«$1»"
STRIP_LEFT(«$2»)
STRIP_LEFT(«$3»)dnl
»)
define(«DECLARE_FUNCTION», «
divert
.BR "«$1»" ()
\- «$2»
.br dnl
divert(«1»)
.SS "STRIP_RIGHT(«$6») $1(DECL_ARGLIST($4))"
STRIP(«$2»)
STRIP(«$3»)
.IP
FORMAT_LIST_ROFF($4)
.P
STRIP(«$5»)dnl
IFNBLANK(«$7», «
.IP
Return
.RB ( "$6" ):
STRIP(«$7»)»
.P
)
STRIP_LEFT(«$8»)
.P
»)
divert(«1»)
.P
.SH DESCRIPTION
divert
»)dnl
ifdef(«EXAMPLES», «
dnl
dnl
dnl	Args
dnl	~~~~
dnl	(1) name, (2) one-line summary, (3) prolog, (4) args,
dnl	(5) arg doc, (6) return type, (7) one-line return text, (8) epilog
DECLARE_FUNCTION(
	«main»,
	«The function that is executed on startup.»,
«
	1. This function must not be static, it's always linked in.

	Each executable needs to define exactly one main function.
», «
	«int argc»,	«usual argument count»,
	«char **argv»,	«usual argument vector»
», «
	2. The arg list may optionally contain an env pointer. In any case
	argc > 0 and argc < INT_MAX.
»,
	«int», «EXIT_SUCCESS or EXIT_FAILURE»,
«
	3. On most systems EXIT_FAILURE is 1 and EXIT_SUCCESS is 0.
»)

DECLARE_FUNCTION(
	«sync»,
	«Commit buffer cache to disk.»,
«
	Causes all buffered modifications to file metadata and data
	to be written to the underlying filesystems.
», «
	«void»,
», «
»,
	«void»,
)

DECLARE_FUNCTION(
	«getchar»,
	«Input of characters and strings.»,
«
	Reads the next character from stdin.
», «
	«void»,
», «
»,
	«int», «the character read»,
«
	On success the return value is an unsigned char cast to
	an int. On end of file or error, EOF is returned.
»
)

DECLARE_FUNCTION(
	«free»,
	«Free dynamic memory.»,
«
	The memory space must have been returned by a previous call to
	malloc(), calloc(), or realloc().
», «
	«void *ptr»,	«free the memory space pointed to by ptr»
», «
	If ptr is NULL, no operation is performed.
»,
	«void»,
)

dnl	Args
dnl	~~~~
dnl	(1) name, (2) one-line summary, (3) prolog, (4) member list, (5) epilog
DECLARE_COMPOUND(
	«struct complex»,
	«Describes a complex number»,
«
», «
	«float re», «real part»,
	«float im», «imaginary part»,
»
)
DECLARE_COMPOUND(
	«struct iovec»,
	«According to POSIX, the <sys/uio.h> header shall define the iovec structure.»,
«
	This structure is employed for the readv() and writev() system calls.

	For example, the readv() system call reads into buffers which is
	described by an array of iovec structures.
», «
	«void *iov_base»,	«start address»,
	«size_t iov_len»,	«number of bytes to transfer»
», «
	Buffers are processed in array order.
»
)
dnl	Args
dnl	~~~~
dnl	(1) name, (2) one-line summary, (3) description
STATEMENT(
	«struct foo_handle»,
	«Opaque structure that describes one connection to the foo subsystem»,
«
	The compiler considers this structure an incomplete type.

	Applications can still declare pointer variables or arrays of pointers
	to this structure, and compare pointers for equality. However, they
	will not be able to de-reference a pointer, and can only change the
	pointer's content by calling some procedure of the library.
»
)
»)
