.\" groff -m man -Thtml <file>
.\" See groff_www(7), groff(7) and groff_man(7)

.\" save current font, switch to monospace font, output, switch back
.de MONO
.nr mE \\n(.f
.ft CW
\\$*
.ft \\n(mE
..

.\" make text on the same line appear alternately in monospace and roman
.de MONO_ROMAN
.  if \\n[.$] \{\
.    ds an-result \&\f[CW]\\$1\f[R]\"
.    shift
.    while (\\n[.$] >= 2) \{\
.      as an-result \/\\$1\f[CW]\,\\$2\f[R]\"
.      shift 2
.    \}
.    if \\n[.$] .as an-result \/\\$1\"
\\*[an-result]
.    ft R
.  \}
..

. if !'\*(.T'html' \
. do ftr CW I
. \}

.TH lopsub 7 "DATE()" GIT_VERSION()
.SH NAME
lopsub \- Long option parser for subcommands

.SH About

.B Lopsub
is an open source library written in
.B C
which aims to ease the task of creating, documenting and parsing
the options of Unix command line utilities. It is suitable for
simple commands as well as complex command line utilities with many
subcommands where each subcommand has its own set of options. Options
and documentation are kept together in a single file which can be
translated to
.B C
code (to be included in the application), or to a manual page.
The library supports single-character short options and GNU-style
long options. The public API is well documented and stable.

To make use of the library, the programmer provides a so-called
.I suite file
which describes the options of the application in an intuitive
syntax. At build time the suite file is translated into
.B C
code by the
.MONO lopsubgen
utility which also ships with the
.B lopsub
package. The generated code defines an instance of an opaque
.B C
structure and exposes a reference to this structure which can be passed to
the
.I option parser
at run time, together with the usual argument vector. The
option parser is part of the
.B lopsub
.IR library ,
so applications need to link with
.MONO_ROMAN -llopsub .
In addition to the option parser, the library offers many more
features. For example, there is a function for merging two different
.I parse results
to generate an effective configuration. This is useful for applications
which can be configured through command line options and via a
config file.

The suite file can also be processed into roff format to create
a manual page. Conversion into html can easily be performed with
tools like
.MONO grohtml
(part of
.BR "GNU roff" )
or
.MONO_ROMAN man2html .

.B Lopsub
does not rely on the system's
.MONO getopt()
or
.MONO getopt_long()
functions of the C library and is thus portable across different
flavors of Unix. It is known to work on
.BR Linux ,
.B NetBSD
and
.BR FreeBSD .

.SH License
The
.B lopsub library
is licensed under the
.B LGPLv3
while the
.MONO lopsubgen
utility is licensed under the
.BR GPLv3 .
The examples and all code generated by the utilities, however, is
licensed under the public domain. You are free to do anything you
like with the generated code, including incorporating it into or
linking it with proprietary software.

.SH Quick Start
Compile and run the minimal example included at the end of
.UR ./lopsub-suite.5.html
lopsub-suite(5)
.UE .
Examine
.MONO lopsubex.c
and
.MONO lopsubex.suite
in the source tree and run the
.MONO lopsubex
command.

.SH API documentation
See
.UR ./lopsub-api.html
.UE .
Alternatively, examine
.MONO lopsub.h
or
.MONO_ROMAN lopsub.h.m4 .

.SH Resources

\(bu
git clone URL:
.UR https://git.tuebingen.mpg.de/lopsub.git
.UE

\(bu
gitweb:
.UR https://git.tuebingen.mpg.de/lopsub.git
.UE

\(bu
The author's homepage:
.UR https://people.tuebingen.mpg.de/maan/
.UE

\(bu
Send feedback to
.MT maan@tuebingen.mpg.de
Andre Noll
.ME

