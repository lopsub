.TH lopsub-suite 5 "DATE()" GIT_VERSION()
.SH NAME
lopsub-suite \- lopsub suite syntax
.SH DESCRIPTION
A
.B lopsub suite
describes the options to a command line utility with zero or more
related subcommands. The
.BR lopsubgen (1)
utility translates a lopsub suite into either C source code or a manual
page. The generated C code can be compiled and linked against the
lopsub library to produce command line parsers for the main command
and all subcommands defined in the suite.

This document explains the format of a lopsub suite. The overall
structure is as follows:

.EX
	[suite mysuite]
	suite directives

	[supercommand sup_cmd]
		command directives for sup_cmd
		[option opt1]
		... (further options)

	[subcommand sub1]
		command directives for sub1
		[option opt_1_to_sub1]
			option directives for opt_1
		... (further options)

	... (further subcommands)

	[section see also]
	... (optional extra section for man page)

	... (further extra sections)
.EE

A suite begins with a
.B [suite]
line which declares the name of the suite. The
.B suite directives,
all subsequent lines up to the first
.B [supercommand]
or
.B [subcommand]
line, state further properties of the suite which are unrelated to
any particular command, for example the version number. All available
suite directives are summarized below.

The
.B [supercommand]
and
.B [subcommand]
lines indicate the beginning of a command of the suite. The part between this
line and the first
.B [option]
line contains the
.B command directives,
for example the purpose and the description of the named command.
See the section on command directives below.

Supercommands and subcommands share the same set of possible command
directives. They differ mainly in the way the documentation is
formatted. There can only be one supercommand but arbitrary many
subcommands. For example, the supercommand could be the name of
the application, and the subcommands could be "load", "save" "info"
and "quit". The subcommand would be passed as the first non-option
argument to the supercommand, followed by options specific to that
subcommand.

Of course it is possible to define no subcommands at all. Conversely,
one can define only subcommands but no supercommand. This makes
sense if the commands are run by some other means, for example in an
interactive session from a command prompt.

Within the command section, an
.B [option]
line starts an option to the current command. It is followed by
.B option directives,
which specify, for example, whether or not the option takes
an argument. All supported option directives are listed in the
corresponding section below.

Further material for man output can be included between the
.B [section]
and
.B [/section]
markers.
This text will not be included in the generated .c and .h files and
is thus also not part of the short and long help available at run
time. The text is not interpreted except that leading whitespace is
stripped from each line. Arbitrary roff source can be included here.

Empty lines and lines starting with a hash character (#) are
ignored.
.SH SUITE DIRECTIVES
Most directives of this section are only relevant for man page output (with
.B aux_info_default
being the exception), they are ignored for C output.
.TP
.B caption
The optional text for an unnumbered section heading at the beginning of the
manual page.
.TP
.B title
Sets the title of the man page. Defaults to the name of the
supercommand. If this is not given and the suite has no supercommand,
the .TH macro to set the title of the man page is omitted.
.TP
.B mansect
Sets the man page section. Defaults to 1 (user commands). Both title
and section are positioned at the left and right in the header line.
.TP
.B date
This text is positioned in the middle of the footer line of the man page. It is
common to set this to the date of the last nontrivial change that was made to
the man page. Defaults to the current date.
.TP
.B version-string
Positioned at the left in the footer line. Defaults to the empty string. The
value of this directive is ignored if a version string is explicitly requested
by passing
.B --version-string
to
.BR losubgen .
.TP
.B manual_title
Centered in the header line. Defaults to "User commands".
.TP
.B introduction
.TQ
.B conclusion
The text enclosed between
.B [introduction]
and
.B [/introduction]
is shown between the supercommand (if any) and the subcommand list.
Concluding remarks after the subcommand list may be added in the same
way with
.B [conclusion]
and
.BR [/conclusion] .
Both texts will become part of the manual page, but are not not part
of the short or long help.
.TP
.B aux_info_prefix
This text is shown at the bottom of each command before the value of the
aux_info directive. If no
.B aux_info
is specified for a command, the prefix is omitted as well. If
.B aux_info_prefix
is not given, the empty string is assumed.
.TP
.B aux_info_default
This text is only used for header output.  The argument for the generated macro
call is set to this value for all commands for which no
.B aux_info
directive is given. If no
.B aux_info_default
directive is given, the value 0 is used as the argument for the macro.
.SH COMMAND DIRECTIVES
.TP
.B purpose
A single line containing the purpose of the command. This text is printed in
the short and long help, and is also included in the man page.
.TP
.B description
.TQ
.B closing
Arbitrary plain text enclosed between
.B [description]
and
.BR [/description] .
The text may be split over multiple lines and paragraphs. The
description of the supercommand (if any) becomes the description of
the manual page, which is shown after the command summary and before
the list of options. The descriptions of all commands are included
in the manual page and in the long help text but not in the short help.

Closing remarks for the command can be added in a similar way by enclosing
plain text between
.B [closing]
and
.BR [/closing] .
This text will be positioned after the option list.
.TP
.B synopsis
Custom synopsis of the command. If this is not given, the synopsis text will be
auto-generated from the options and the value of the
.B non-opts-name
directive.
.TP
.B non-opts-name
Name of the command line arguments which are not related to any option,
for example file names. This text will be included in the automatically
generated synopsis text.

If this is not given, the command is assumed to take no arguments other than
the specified options and their arguments. For such commands the attempt to
parse an argv[] vector fails if it contains further non-option arguments.
.TP
.B aux_info
This directive is special because its value is not included in the generated .c
file but in the header file. More precisely, the preprocessor macro
.B \%LSG_SUITENAME_AUX_INFOS
will be defined which expands to a series of macro calls to
.B \%LSG_SUITENAME_AUX_INFO(val_n),
one for each command of the suite, where
.B val_n
is the (unquoted) value of the
.B aux_info
directive of the nth command. Commands for which no
.B aux_info
directive was specified receive a value of zero. The
.B \%LSG_SUITENAME_AUX_INFO
macro is supposed to be defined by the application.
Hence it is up to the application to make the expansion of
.B \%LSG_SUITENAME_AUX_INFOS
a valid C construct.

The value, if specified, is also copied to the man page at the end of the
section for the command.

.SH OPTION DIRECTIVES
.TP
.B short_opt
The optional single-letter equivalent for the option. If this is
specified, the option may be given either in the GNU-style long
option format with two leading dashes or in the short form with a
single dash. Otherwise only the long form will be accepted. As usual,
multiple short option flags may be combined.
.TP
.B summary
A single line which summarizes the option. This text is included in
both the short and the long help and in the man page. Defaults to
the empty string.
.TP
.B typestr
A description for the type of the values for the option. The given text
is printed in the synopsis of the command, which is part of the short
and the long help and the man page. It defaults to the string "val".

This directive is ignored for flag options (options without an argument).
.TP
.B arg_info
This directive determines whether the option takes an argument. The
possible values are
.B no_arg, required_arg
and
.B optional_arg
which indicate, respectively, that the option takes no argument at all,
an argument which is mandatory, or an argument which may be omitted. The
default value is
.B no_arg.
Hence an option works as a flag if the
.B arg_info
directive is not given.

Note that arguments to options which take an optional argument must
be given as --foo=bar rather than --foo bar because the latter form
would be ambiguous.
.TP
.B arg_type
For flag options this directive should be set to
.B none,
or not set at all. For options which take an argument, the value of the directive
determines the type of the argument.
Possible values are
.B string
for options which take a string argument, and
.B int32,
.B uint32.
.B int64,
.B uint64,
for options which take a numeric argument.
.TP
.B flag
Lopsub maintains for each option a bitmask which contains the value
of each possible flag for this option. Flags may be accumulated by
defining multiple flag directives in the suite. Note there is no
equal sign between the
.B flag
directive and its value.

The following flags are defined.
.RS
.B
.IP multiple
This flag instructs the lopsub library to keep track of all given
arguments to an option, not just one as for ordinary options. This
is only relevant for options which take an (optional or required)
argument.
.B
.IP required
Instruct the parser to fail if this option is not given in the
argument vector. If an option may be given at the command line or
in the config file, this flag should be avoided because the command
line argv vector will not be parsed successfully if the option is
only given in the config file. The recommended way to deal with
this situation is to parse command line and config file separately,
then merge the two parse results and check in the application if the
option is given in the merged parse result.

There is another disadvantage of this flag: if the parser fails due
to a missing option that was declared required, it is not possible to
detect if other options were given. For example, if the suite defines
the --help option, and the application is executed with this option
only, the parser will still return a parse error.
.B
.IP ignored
This flag indicates that the current option is in fact not a real option.
In particular,
.B name
and
.B short_opt
are both ignored. The purpose of this flag is to add additional
information for the help output and the man page.
.RE
.TP
.B values
Create an enumerable option.

Enumerable options take one out of a fixed set of possible values
which are predefined in the suite. Such options are always of type
string. It is an error if a different argument type was specified or
if the option was defined to not take an argument.

The syntax for the array of values is

.EX
	values = {ID_0 = "string_0", ..., ID_N = "string_N"}
.EE

For each option for which the
.B values
directive was specified, the lopsubgen command generates a C
enumeration which contains the given identifiers. This allows
referring to each possible value through a numeric constant.
.TP
.B default_val
This directive makes only sense for options which take an argument. For
such options it defines the value the option parser provides
automatically if the option is not given in the argument vector.

If no
.B default_val
is specified in the suite, and the option is not given in the argument
vector either, the implied value depends on the argument type. For
numeric options, a value of zero is assumed. For options which take
a string argument, a NULL pointer is returned. For enum options,
the first possible value (index zero) is taken as the default.
.TP
.B help
The detailed, multi-line help text for the current option. Included
in the man page and the long help.
.SH EXAMPLE
A minimal suite and the corresponding "application".

The suite file
.IR hello.suite :

.EX
	[suite hello]
	[supercommand hello]
		purpose = hello world
		[option world]
			summary = add "world"
.EE

The "application"
.IR hello.c :

.EX
	#include <stdio.h> /* printf(3) */
	#include <stdlib.h> /* exit(3) */
	#include <lopsub.h>
	#include "hello.lsg.h"

	int main(int argc, char **argv)
	{
		struct lls_parse_result *lpr;
		const struct lls_opt_result *result;
		const struct lls_command *cmd = lls_cmd(0, hello_suite);
		int ret;

		ret = lls_parse(argc, argv, cmd, &lpr, NULL);
		if (ret < 0) {
			fprintf(stderr, "%s\\n", lls_strerror(-ret));
			exit(1);
		}
		printf("hello");
		result = lls_opt_result(LSG_HELLO_HELLO_OPT_WORLD, lpr);
		if (lls_opt_given(result))
			printf(" world");
		printf("\\n");
		exit(0);
	}
.EE

Generate
.I hello.lsg.c
and
.IR hello.lsg.h :

.EX
	$ lopsubgen --gen-c --gen-h < hello.suite
.EE

Compile
.I hello.c
and
.I hello.lsg.c
to produce the
.I hello
executable:

.EX
	$ cc hello.c hello.lsg.c -o hello -llopsub
.EE

Run it:

.EX
	$ ./hello
	hello
	$ ./hello --world
	hello world
	$ ./hello -w
	option not recognized
	$ ./hello 42
	invalid number of arguments
.EE

Generate and examine the manual page:

.EX
	$ lopsubgen --gen-man < hello.suite
	$ man -l ./hello.lsg.man
.EE

.SH SEE ALSO
.BR lopsubgen (1),
.BR lopsub (7),
.BR lopsubex (1),
.BR gengetopt (1),
.BR getopt (1),
.BR getopt (3)
