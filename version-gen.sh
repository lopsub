#!/bin/sh

version_file='version.c'
ver='unnamed_version'
# First try git, then gitweb, then default.
if [ -e '.git' -o -e '../.git' ]; then
	git_ver=$(git describe --abbrev=4 HEAD 2>/dev/null)
	[ -z "$git_ver" ] && git_ver="$ver"
	# update stat information in index to match working tree
	git update-index -q --refresh > /dev/null
	# if there are differences (exit code 1), the working tree is dirty
	git diff-index --quiet HEAD || git_ver=$git_ver-dirty
	ver=$git_ver
elif [ -f debian/changelog ]; then
	ver="$(sed -ne '1s/.*(\(.*\)-.*/\1/1; tx; d; :x; p' debian/changelog)"
elif [ "${PWD%%-*}" = 'lopsub-' ]; then
	ver=${PWD##*/lopsub-}
fi
ver=${ver#v}

echo "$ver"

# update version file if necessary
content="const char *lls_version(void) {return \"$ver\";};"
[ -r "$version_file" ] && echo "$content" | cmp -s - $version_file && exit 0
echo >&2 "new git version: $ver"
echo "$content" > $version_file
